<a href="<?= $this->url->to('UserNotesAndTodosController', 'showCreateModal', array('plugin' => 'Notes')) ?>" class="js-modal-medium" title="New note">
    <i class="fa fa-plus fa-fw js-modal-medium" aria-hidden="true"></i><?= t('New note') ?>
</a>