<?php

return array(
    'My Notes' => 'Minhas notas',
    'No notes found' => 'Nenhuma nota encontrada',
    'New note' => 'Nova nota',
    'Remove a note' => 'Remover uma nota',
    'Do you really want to remove this note: "%s"?' => 'Você realmente deseja remover esta nota ? : "%s"?',
    'Remove filters' => 'Remover filtros',
    'Untagged' => 'Sem tag',
    'Default filters' => 'Filtro padrão',
    'Created On Date' => 'Data de criação',
    'Last Modified Date' => 'Última data de modificação',
    'Note Title' => 'Título da nota',
    'Add new tag' => 'Adicionar nova tag',
    'Remove a tag' => 'Remover uma tag',
    'Do you really want to remove this tag: "%s"?' => 'Você realmente deseja remover essa tag: "%s"?',
    'Edit a tag' => 'Editar uma tag',
    'User tags' => 'Tags do usuário',
    'There are no user tags at the moment.' => 'Não existem tags no momento',
    'Action' => 'Ações',
    'Edit' => 'Editar',
    'Remove' => 'Remover',
    'Tags Management' => 'Gerenciar tags'
);
